##########################################
# Signal Conditioning Board Quality Control Scripts
# Requires PySerialComm
# Carlos.Solans@cern.ch
# Samuel.Rodriguez.Ochoa
##########################################

tdaq_package()

# Test scripts

tdaq_add_scripts(share/*.py)

